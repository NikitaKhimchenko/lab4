package org

import java.lang.IllegalArgumentException

/**
 * filling in the resulting matrix
 * @param matrix - matrix that will be changed
 * @param dim - the dimension of matrix
 */
fun fillMatrix(matrix: Matrix, dim: Int) {
    if (dim <= 0) throw IllegalArgumentException("dim should be > 0")
    val resultBuilder = DiagonalMatrixBuilder()
    resultBuilder.buildSize(dim)
    IntRange(0, dim * dim - 1).forEach {
        resultBuilder.buildCell(matrix)
    }
}
