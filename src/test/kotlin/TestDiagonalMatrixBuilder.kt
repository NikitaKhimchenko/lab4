package org

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestDiagonalMatrixBuilder {

    @Test
    fun `Dim 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, 0)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Dim lower than 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, -1)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Matrix 1 x 1`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 1)

        Mockito.verify(m).set(0, 0, 1)
    }

    @Test
    fun `Matrix 4 x 4`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 4)
//      10, 4, 3, 1,
//      11, 9, 5, 2,
//      15, 12, 8, 6,
//      16, 14, 13, 7
        Mockito.verify(m).set(0, 3, 1)
        Mockito.verify(m).set(1, 3, 2)
        Mockito.verify(m).set(0, 2, 3)
        Mockito.verify(m).set(0, 1, 4)
        Mockito.verify(m).set(1, 2, 5)
        Mockito.verify(m).set(2, 3, 6)
        Mockito.verify(m).set(3, 3, 7)
        Mockito.verify(m).set(2, 2, 8)
        Mockito.verify(m).set(1, 1, 9)
        Mockito.verify(m).set(0, 0, 10)
        Mockito.verify(m).set(1, 0, 11)
        Mockito.verify(m).set(2, 1, 12)
        Mockito.verify(m).set(3, 2, 13)
        Mockito.verify(m).set(3, 1, 14)
        Mockito.verify(m).set(2, 0, 15)
        Mockito.verify(m).set(3, 0, 16)
    }

    @Test
    fun `Matrix 3 x 3`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 3)
        //    4,  3,  1,
        //    8,  5,  2,
        //    9,  7,  6,
        Mockito.verify(m).set(0, 2, 1)
        Mockito.verify(m).set(1, 2, 2)
        Mockito.verify(m).set(0, 1, 3)
        Mockito.verify(m).set(0, 0, 4)
        Mockito.verify(m).set(1, 1, 5)
        Mockito.verify(m).set(2, 2, 6)
        Mockito.verify(m).set(2, 1, 7)
        Mockito.verify(m).set(1, 0, 8)
        Mockito.verify(m).set(2, 0, 9)
    }
}
