package org

/**
 * Builder for diagonal matrix collects information about the matrix to be built and builds it element-by-element.
 * Example of a 4x4 matrix that will be built if you call buildCell 16 times with values from 1 to 16:
 *  10, 4, 3, 1,
 *  11, 9, 5, 2,
 *  15, 12, 8, 6,
 *  16, 14, 13, 7
 */
class DiagonalMatrixBuilder {
    private var dim: Int = 0
    private var countElement: Int = 1
    private var diagonalNumber = 1
    private var sum = 1
    private var elementNumber = 1
    private var lastSum = 0
    private fun getI(): Int =
        diagonalNumber - elementNumber - (diagonalNumber - dim) * diagonalNumber.div(dim)

    private fun getJ(): Int =
        elementNumber - 1 + (diagonalNumber - dim) * diagonalNumber.div(dim)

    /**
     * Build size for matrix
     * @param dim dimension of the matrix
     */
    fun buildSize(dim: Int) {
        this.dim = dim
    }

    /**
     * Selects a matrix cell based on the number of the current element and writes the value to it
     * @param matrix the matrix into which the value will be written
     */
    fun buildCell(matrix: Matrix) {
        if (countElement > sum) {
            diagonalNumber++
            lastSum = sum
            if (diagonalNumber <= dim) {
                sum = diagonalNumber * (diagonalNumber + 1) / 2
            } else {
                sum += dim - diagonalNumber.rem(dim)
            }
        }
        elementNumber = countElement - lastSum
        if (diagonalNumber % 2 == 0) {
            matrix.set(getI(), dim - getJ() - 1, countElement)
        } else {
            matrix.set(getJ(), dim - getI() - 1, countElement)
        }
        countElement++
    }
}
